import re
import ts3.commands
import inspect

server_query = '''
banadd                      | create a ban rule
banclient                   | ban a client
bandel                      | delete a ban rule
bandelall                   | delete all ban rules
banlist                     | list ban rules on a virtual server
bindinglist                 | list IP addresses used by the server instance
channeladdperm              | assign permission to channel
channelclientaddperm        | assign permission to channel-client combi
channelclientdelperm        | remove permission from channel-client combi
channelclientpermlist       | list channel-client specific permissions
channelcreate               | create a channel
channeldelete               | delete a channel
channeldelperm              | remove permission from channel
channeledit                 | change channel properties
channelfind                 | find channel by name
channelgroupadd             | create a channel group
channelgroupaddperm         | assign permission to channel group
channelgroupclientlist      | find channel groups by client ID
channelgroupcopy            | copy a channel group
channelgroupdel             | delete a channel group
channelgroupdelperm         | remove permission from channel group
channelgrouplist            | list channel groups
channelgrouppermlist        | list channel group permissions
channelgrouprename          | rename a channel group
channelinfo                 | display channel properties
channellist                 | list channels on a virtual server
channelmove                 | move channel to new parent
channelpermlist             | list channel specific permissions
clientaddperm               | assign permission to client
clientdbdelete              | delete client database properties
clientdbedit                | change client database properties
clientdbfind                | find client database ID by nickname or UID
clientdbinfo                | display client database properties
clientdblist                | list known client UIDs
clientdelperm               | remove permission from client
clientedit                  | change client properties
clientfind                  | find client by nickname
clientgetdbidfromuid        | find client database ID by UID
clientgetids                | find client IDs by UID
clientgetnamefromdbid       | find client nickname by database ID
clientgetnamefromuid        | find client nickname by UID
clientgetuidfromclid        | find client UID by client ID
clientinfo                  | display client properties
clientkick                  | kick a client
clientlist                  | list clients online on a virtual server
clientmove                  | move a client
clientpermlist              | list client specific permissions
clientpoke                  | poke a client
clientsetserverquerylogin   | set own login credentials
clientupdate                | set own properties
complainadd                 | create a client complaint
complaindel                 | delete a client complaint
complaindelall              | delete all client complaints
complainlist                | list client complaints on a virtual server
custominfo                  | display custom client properties
customsearch                | search for custom client properties
ftcreatedir                 | create a directory
ftdeletefile                | delete a file
ftgetfileinfo               | display details about a file
ftgetfilelist               | list files stored in a channel filebase
ftinitdownload              | init a file download
ftinitupload                | init a file upload
ftlist                      | list active file transfers
ftrenamefile                | rename a file
ftstop                      | stop a file transfer
gm                          | send global text message
help                        | read help files
hostinfo                    | display server instance connection info
instanceedit                | change server instance properties
instanceinfo                | display server instance properties
logadd                      | add custom entry to log
login                       | authenticate with the server
logout                      | deselect virtual server and log out
logview                     | list recent log entries
messageadd                  | send an offline message
messagedel                  | delete an offline message from your inbox
messageget                  | display an offline message from your inbox
messagelist                 | list offline messages from your inbox
messageupdateflag           | mark an offline message as read
permfind                    | find permission assignments by ID
permget                     | display client permission value for yourself
permidgetbyname             | find permission ID by name
permissionlist              | list permissions available
permoverview                | display client permission overview
permreset                   | delete all server and channel groups and
                           | restore default permissions
privilegekeyadd             | creates a new privilege key
privilegekeydelete          | delete an existing privilege key
privilegekeylist            | list all existing privilege keys on this server
privilegekeyuse             | use a privilege key
quit                        | close connection
sendtextmessage             | send text message
servercreate                | create a virtual server
serverdelete                | delete a virtual server
serveredit                  | change virtual server properties
servergroupadd              | create a server group
servergroupaddclient        | add client to server group
servergroupaddperm          | assign permissions to server group
servergroupautoaddperm      | globally assign permissions to server groups
servergroupautodelperm      | globally remove permissions from server group
servergroupclientlist       | list clients in a server group
servergroupcopy             | create a copy of an existing server group
servergroupdel              | delete a server group
servergroupdelclient        | remove client from server group
servergroupdelperm          | remove permissions from server group
servergrouplist             | list server groups
servergrouppermlist         | list server group permissions
servergrouprename           | rename a server group
servergroupsbyclientid      | get all server groups of specified client
serveridgetbyport           | find database ID by virtual server port
serverinfo                  | display virtual server properties
serverlist                  | list virtual servers
servernotifyregister        | register for event notifications
servernotifyunregister      | unregister from event notifications
serverprocessstop           | shutdown server process
serverrequestconnectioninfo | display virtual server connection info
serversnapshotcreate        | create snapshot of a virtual server
serversnapshotdeploy        | deploy snapshot of a virtual server
serverstart                 | start a virtual server
serverstop                  | stop a virtual server
servertemppasswordadd       | create a new temporary server password
servertemppassworddel       | delete an existing temporary server password
servertemppasswordlist      | list all existing temporary server passwords
setclientchannelgroup       | set a clients channel group
tokenadd                    | alias for privilegekeyadd
tokendelete                 | alias for privilegekeydelete
tokenlist                   | alias for privilegekeylist
tokenuse                    | alias for privilegekeyuse
use                         | select virtual server
version                     | display version information
whoami                      | display current session info
'''

client_query = '''
help                        | read help files
quit                        | close connection
use                         | select server connection handler
auth                        | authenticate telnet connection with users API key

banadd                      | add a new ban rule to the server
banclient                   | ban a client from the server 
bandelall                   | delete all active ban rules
bandel                      | delete an active ban rule from the server
banlist                     | list all active ban rules
channeladdperm              | add a permission to a channel
channelclientaddperm        | add a channel-client permisison to a client and specified channel id
channelclientdelperm        | delete a channel-client permisison from a client and specified channel id
channelclientlist           | displays a list of clients that are in the channel specified by the cid parameter
channelclientpermlist       | list all assigned channel-client permisisons for a client and specified channel id
channelconnectinfo          | channel connect information
channelcreate               | create a channel
channeldelete               | delete a channel
channeldelperm              | delete a from a channel
channeledit                 | edit a channel
channelgroupadd             | create a channel group
channelgroupaddperm         | add a permission to a channel group
channelgroupclientlist      | list all assigned channel groups for the specified channel id
channelgroupdel             | delete a channel group
channelgroupdelperm         | delete a permission from a channel group
channelgrouplist            | list all available channel groups
channelgrouppermlist        | list all assigned permissions from a channel group
channellist                 | list of all channels
channelmove                 | assign a new parent channel to a channel
channelpermlist             | list all assigned permissions for a channel
channelvariable             | retrieve specific information about a channel
clientaddperm               | add a permission to a clientDBID
clientdbdelete              | delete a client from the server database
clientdbedit                | edit a clients properties identified by clientDBID
clientdblist                | list all clients stored in the server database
clientdelperm               | delete a permission from a clientDBID
clientgetdbidfromuid        | get the clientDBIDs for a certain client unique id
clientgetids                | get the clientIDs for a certain client unique id
clientgetnamefromdbid       | get the nickname from a client database id
clientgetnamefromuid        | get the nickname from a client unique id
clientgetuidfromclid        | get the unique id from a clientID
clientkick                  | kick a client
clientlist                  | list known clients
clientmove                  | move a client or switch channel ourself
clientmute                  | mute all voice data from a client
clientunmute                | unmute a previously muted client
clientnotifyregister        | register to receive client notifications
clientnotifyunregister      | unregister from receiving client notifications
clientpermlist              | list all assigned permissions from a clientDBID
clientpoke                  | poke a client
clientupdate                | set personal client variables, like your nickname
clientvariable              | retrieve specific information about a client
complainadd                 | submit a complaint about a clientDBID
complaindelall              | delete all complaints from a clientDBID
complaindel                 | delete a complaint from the server
complainlist                | list all complaints from a server or for a clientDBID
currentschandlerid          | server connection handler ID of current server tab
ftcreatedir                 | create a new directory
ftdeletefile                | delete one or more files
ftgetfileinfo               | get informations about the specified file
ftgetfilelist               | list all files for the specified channel and filepath
ftinitdownload              | initialise a filetransfer download
ftinitupload                | initialise a filetransfer upload
ftlist                      | get a list of all file transfers currently running on the server  notifyfiletransferlist
ftrenamefile                | rename the specified file
ftstop                      | stop an running file transfer progress
hashpassword                | create a password hash
messageadd                  | send an offline message to a clientDBID
messagedel                  | delete an existing offline message from your inbox
messageget                  | display an existing offline message from your inbox   
messagelist                 | list all offline messages from your inbox
messageupdateflag           | mark or unmark an offline message as read
permoverview                | list all assigned permissons
sendtextmessage             | send a chat message
serverconnectinfo           | server connect information
serverconnectionhandlerlist | list available server connection handlers
servergroupaddclient        | add a client to a server group
servergroupadd              | create a server group
servergroupaddperm          | add a permission to a server group
servergroupclientlist       | list all client database ids from a server group
servergroupdelclient        | delete a client from a server group
servergroupdel              | delete a server group
servergroupdelperm          | delete a permission from a server group
servergrouplist             | get a list of server groups
servergrouppermlist         | list all assigned permission from a server group
servergroupsbyclientid      | get all assigned server groups from a clientDBID
servervariable              | retrieve specific information about a server
setclientchannelgroup       | assign a channel group to a client database id
tokenadd                    | add a token to a server- or channel group
tokendelete                 | delete an existing token from the server
tokenlist                   | lists all tokens available on the server
tokenuse                    | use a token to gain access to the server
verifychannelpassword       | check if we know the current password of a channel
verifyserverpassword        | check if we know the current server password
whoami                      | display information about ourself
'''


def create_cmd_list(src_text: str):
    return [cmd for cmd in re.findall('^\s*(\w+).*', src_text, flags=re.RegexFlag.MULTILINE)]


cq_cmdl = set(create_cmd_list(client_query))
sq_cmdl = set(create_cmd_list(server_query))

common_cmdl = sorted(set.intersection(cq_cmdl, sq_cmdl))
cq_unique_cmdl = sorted(cq_cmdl.difference(sq_cmdl))
sq_unique_cmdl = sorted(sq_cmdl.difference(cq_cmdl))


def assert_members(cls, filter_base_members, *cmdls):
    # Create list of members of base classes.
    base_members = []
    if filter_base_members:
        for bs in cls.__bases__:
            base_members += [cmdn[0] for cmdn in inspect.getmembers(bs)]

    # Create list of members of target class, filter base members out if list was created.
    members = [cmdn[0] for cmdn in inspect.getmembers(cls)  if cmdn[0] not in base_members]

    cmdl = []
    for p_cmdl in cmdls:
        cmdl += p_cmdl

    cmdl = set(cmdl)
    for cmd in cmdl:
        if cmd not in members:
            print(f'{cls.__name__} misses {cmd}')
        else:
            members.remove(cmd)

    if members != []:
        print(f'{cls.__name__} implements {members} as well.')


assert_members(ts3.commands.TS3CommonCommands, True, common_cmdl)
assert_members(ts3.commands.TS3ServerCommands, True, sq_unique_cmdl)
assert_members(ts3.commands.TS3ClientCommands, True, cq_unique_cmdl)
