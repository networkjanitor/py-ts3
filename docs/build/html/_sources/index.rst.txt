Welcome to PyTS3's documentation!
=================================

Content
-------

.. toctree::
   :maxdepth: 1

   installation
   api/index
   examples/index
   changelog
   faq
   contribute
   license

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

What is PyTS3?
--------------

It's a small package that contains a Python 3.2+ API for

   * **TS3 Server Query Events**,
   * the **TS3 Server Queries API**
   * the **TS3 File Transfer Interface**,
   * the **TS3 Client Query API**.
