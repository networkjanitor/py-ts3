.. _contribute:   
   
Contribute
==========

.. image:: /_static/gitlab_square_small.png
   :height: 40
   :align: right

**This project needs your help!**
Please help to improve this application and fork the repository on
`GitLab. <https://gitlab.com/networkjanitor/py-ts3>`_

Bug reports
-----------

When you found a bug, please create a bug report on 
`GitLab/Issues. <https://gitlab.com/networkjanitor/py-ts3/issues>`_

If you know how to fix the bug, you're welcome to send a *pull request.*

Code / Enhancements
-------------------

If you want to contribute to the code or you have suggestions how we could
improve the code, then tell me about it.

Spelling Mistakes
-----------------

I guess this documentation and the source code contains a lot of spelling
mistakes. Please help to reduce them.