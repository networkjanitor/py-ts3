.. _installation:

Installation
============

*ts3query* is registered on PyPi, so you are done with:

.. code-block:: bash

   $ pip3 install ts3query

You can update the library then with:

.. code-block:: bash

   $ pip3 install --upgrade ts3query
