Mute/Unmute
===========

Download: :download:`hello_bot.py </../../ts3/examples/mute_unmute.py>`

.. literalinclude:: /../../ts3/examples/mute_unmute.py
